var gulp = require('gulp'),
    webpack = require('webpack');

gulp.task('scripts',function(callback){
   webpack(require("../../webpack.config.js"),function(err,stats){
       if(err){
           console.log(err.toString());
       }
        console.log(stats.toString());
       callback();/* Instead of return to stop asynchronous thread we passes the name of the function in the argument of function*/
   }); 
});